import { newSpecPage } from '@stencil/core/testing';
import { TelekomLogo } from './t-logo';

describe('component prop snapshots', () => {
  let page: any;
  let component: any;

  beforeEach(async () => {
    page = await newSpecPage({
      components: [TelekomLogo],
      html: `<div></div>`,
    });
    component = page.doc.createElement('rating-stars');
    page.root.appendChild(component);
  });

  let color: 'magenta' | 'white' = 'magenta';
  let language: 'DE' | 'EN' | 'CZ' | 'HR' | 'HU' | 'ME' | 'MK' | 'RO' | 'SK' | 'Claim Off' = 'EN';
  let height: number = 36;
  let link: boolean = false;
  let linkname: string = 'https://www.telekom.de/start';

  describe('all props', () => {
    it('are not set', async () => {
      await page.waitForChanges();
      expect(page.rootInstance.color).toBe(color);
      expect(page.rootInstance.language).toBe(language);
      expect(page.rootInstance.height).toBe(height);
      expect(page.rootInstance.link).toBe(link);
      expect(page.rootInstance.linkname).toBe(linkname);

      expect(page.root).toMatchSnapshot();
    });
  });
});
