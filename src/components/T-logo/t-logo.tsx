import { Component, h, Prop, Host } from '@stencil/core';
import logo_de_claim from './T_Logo_DE.svg';
import logo_en_claim from './T_Logo_EN.svg';
import logo_cz_claim from './T_Logo_CZ.svg';
import logo_hr_claim from './T_Logo_HR.svg';
import logo_hu_claim from './T_Logo_HU.svg';
import logo_me_claim from './T_Logo_ME.svg';
import logo_ro_claim from './T_Logo_RO.svg';
import logo_sk_claim from './T_Logo_SK.svg';
import logo_claimOff from './T_Logo_OFF.svg';

const colorVariants = {
  white: '#ffffff',
  magenta: '#e20074',
};

const svgs = {
  de: logo_de_claim,
  en: logo_en_claim,
  cz: logo_cz_claim,
  hr: logo_hr_claim,
  hu: logo_hu_claim,
  me: logo_me_claim,
  ro: logo_ro_claim,
  sk: logo_sk_claim,
  claimOff: logo_claimOff,
};

@Component({
  tag: 'scale-t-logo',
  styleUrl: './t-logo.css',
  shadow: true,
})
export class TelekomLogo {
  /** (optional) variant/color of the logo text and logo */
  @Prop({ reflect: true }) variant: 'magenta' | 'white' = 'magenta'; //TODO: Transparent
  /** (optional) language of the logo text/ claimOff showes just the T Logo */
  @Prop() language: 'de' | 'en' | 'cz' | 'hr' | 'hu' | 'me' | 'mk' | 'ro' | 'sk' | 'claimOff' = 'en';
  /** (optional) height of the Logo itself */
  @Prop() size: number = 36;
  /** (optional) link */
  @Prop() href: string = '';
  /** (optional) possibility for adding a onClick Event */
  @Prop() clickHandler: any;

  getLogo() {
    return this.href === '' ? (
      <div class="logo--logo-container" innerHTML={svgs[this.language]} onClick={this.clickHandler}></div>
    ) : (
      <a href={this.href}>
        <div class="logo--logo-container" innerHTML={svgs[this.language]} onClick={this.clickHandler}></div>
      </a>
    );
  }

  styles() {
    return `:host {
      --logo-size: ${this.size}px;
      --logo-background-color: ${this.variant === 'magenta' ? '#ffffff' : '#e20074'}; 
      --logo-color: ${colorVariants[this.variant]}
    }`;
  }

  render() {
    return (
      <Host>
        <style>{this.styles()}</style>
        <div class="logo">{this.getLogo()}</div>
      </Host>
    );
  }
}
