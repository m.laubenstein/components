import { newSpecPage, SpecPage } from '@stencil/core/testing';
import { RatingStars } from './rating-stars';
/**
 * - (Optional Test) Testing if Lifecyclefunction componentWillLoad() set both Props into the states
 */
it('componentWillLoad set all states valid', () => {
  const component = new RatingStars();
  const starsProp = 5;
  const selectedStarsProp = 2;
  component.stars = starsProp;
  component.selectedStars = selectedStarsProp;
  component.componentWillLoad();
  expect(component.starValue).toBe(starsProp);
  expect(component.selectedStarValue).toBe(selectedStarsProp);
});
/**
 * - Testing if all Props are resulting in a valid output of the component
 * - Testing if the snapshots of the generated component are valid
 */
describe('component prop snapshots', () => {
  let page: any;
  let component: any;

  beforeEach(async () => {
    page = await newSpecPage({
      components: [RatingStars],
      html: `<div></div>`,
    });
    component = page.doc.createElement('rating-stars');
    page.root.appendChild(component);
  });

  //Initialization of the props as variables
  let stars: number = 5;
  let selectedStars: number = 0;
  let rating: number;
  let interactive: boolean = true;
  let size: 'small' | 'large';
  let activityState: 'active' | 'disable' = 'active';
  let spaceBetweenStars: string = '2';
  let individualSize: string;

  describe('all props', () => {
    it('are not set', async () => {
      await page.waitForChanges();
      expect(page.rootInstance.stars).toBe(stars);
      expect(page.rootInstance.selectedStars).toBe(selectedStars);
      expect(page.rootInstance.rating).toBe(rating);
      expect(page.rootInstance.interactive).toBe(interactive);
      expect(page.rootInstance.size).toBe(size);
      expect(page.rootInstance.activityState).toBe(activityState);
      expect(page.rootInstance.spaceBetweenStars).toBe(spaceBetweenStars);
      expect(page.rootInstance.individualSize).toBe(individualSize);

      expect(page.root).toMatchSnapshot();
    });
    it('are set', async () => {
      const setStars = 4;
      const setSelectedStars = 3;
      const setInteractive = false;
      const setSize = 'large';
      const setActivityState = 'disable';
      const setSpaceBetweenStars = '4';
      const setIndividualSize = '3';

      component.stars = setStars;
      component.selectedStars = setSelectedStars;
      component.interactive = setInteractive;
      component.size = setSize;
      component.activityState = setActivityState;
      component.spaceBetweenStars = setSpaceBetweenStars;
      component.individualSize = setIndividualSize;

      await page.waitForChanges();
      expect(page.rootInstance.stars).toBe(setStars);
      expect(page.rootInstance.selectedStars).toBe(setSelectedStars);
      expect(page.rootInstance.interactive).toBe(setInteractive);
      expect(page.rootInstance.size).toBe(setSize);
      expect(page.rootInstance.activityState).toBe(setActivityState);
      expect(page.rootInstance.spaceBetweenStars).toBe(setSpaceBetweenStars);
      expect(page.rootInstance.individualSize).toBe(setIndividualSize);

      expect(page.root).toMatchSnapshot();
    });
  });
});
/**
 * - Testing the setCurrentValue() function for valid return value respectively correct setting of the states/props
 */
describe('setCurrentStarValue function', () => {
  it('values are right', () => {
    let component = new RatingStars();
    component.setCurrentStarValue(3, 4);
    expect(component.starValue).toBe(3);
    expect(component.selectedStarValue).toBe(4);
  });
});
/**
 * - Testing the setRating() function for valid return value respectively correct setting of the states/props
 */
describe('setRating function', () => {
  it('values are right', () => {
    let component = new RatingStars();
    component.setRating(3);
    expect(component.rating).toBe(3);
    expect(component.ratingSet).toBe(true);
  });
});
/**
 * - Testing the setStyle() function for the right return value with right parameters for the scaling of the stars
 */
describe('setStyle function', () => {
  it('individualSize', () => {
    let component = new RatingStars();
    component.individualSize = '2px';
    expect(component.styles()).toMatchSnapshot();
  });
  it('size', () => {
    let component = new RatingStars();
    component.size = 'large';
    expect(component.styles()).toMatchSnapshot();
  });
  it('spaceBetweenStars and individualSize', () => {
    let component = new RatingStars();
    component.spaceBetweenStars = '3px';
    component.size = 'large';
    expect(component.styles()).toMatchSnapshot();
  });
  it('spaceBetweenStars and size', () => {
    let component = new RatingStars();
    component.spaceBetweenStars = '3px';
    component.size = 'large';
    expect(component.styles()).toMatchSnapshot();
  });
});
/**
 * - Testing the startAction() functions, of the switch case, for valid return value respectively correct setting of the states/props
 */
describe('startAction function', () => {
  const numbOfAllStars = 5;
  const numbOfFilledStars = 2;
  it('onMouseOver works', () => {
    let component = new RatingStars();
    const actionName = 'onMouseOver';
    component.handleMouseEvent(actionName, numbOfAllStars, numbOfFilledStars);
    expect(component.starValue).toBe(numbOfAllStars);
    expect(component.selectedStarValue).toBe(numbOfFilledStars);
  });
  it('onMouseLeave works with rating not set', () => {
    let component = new RatingStars();
    const ratingSet = false;
    const actionName = 'onMouseLeave';
    component.ratingSet = ratingSet;
    component.handleMouseEvent(actionName, numbOfAllStars, numbOfFilledStars);
    expect(component.starValue).toBe(numbOfAllStars);
    expect(component.selectedStarValue).toBe(0); // "0" because no "selectedStarValue" or "rating" was set before, so it is still "0"
  });
  it('onMouseLeave works with rating set', () => {
    let component = new RatingStars();
    const ratingSet = true;
    const rating = 3;
    const actionName = 'onMouseLeave';
    component.rating = rating;
    component.ratingSet = ratingSet;
    component.handleMouseEvent(actionName, numbOfAllStars, numbOfFilledStars);
    expect(component.starValue).toBe(numbOfAllStars);
    expect(component.selectedStarValue).toBe(3); // "3" because "rating" was set to "3"
  });
  it('onClick works', () => {
    let component = new RatingStars();
    const actionName = 'onClick';
    component.handleMouseEvent(actionName, numbOfAllStars, numbOfFilledStars);
    expect(component.starValue).toBe(numbOfAllStars);
    expect(component.selectedStarValue).toBe(numbOfFilledStars);
    expect(component.ratingSet).toBe(true);
    expect(component.rating).toBe(component.selectedStarValue);
  });
});
/**
 * - Testing if "interactive = false" disable the startAction() functionality
 */
describe('negativ test', () => {
  const numbOfAllStars = 5;
  const numbOfFilledStars = 2;
  it('element not interactive', () => {
    let component = new RatingStars();
    component.interactive = false;
    const actionName = 'onMouseOver';
    component.handleMouseEvent(actionName, numbOfAllStars, numbOfFilledStars);
    expect(component.starValue).not.toBe(numbOfAllStars);
    expect(component.selectedStarValue).not.toBe(numbOfFilledStars);
  });
});
/**
 * - Testing mouse events of the <span>
 */
describe('mouse events', () => {
  it('onMouseOver', async () => {
    const page = await newSpecPage({
      components: [RatingStars],
      html: `<rating-stars stars="3" selected-stars="0"></rating-stars>`,
    });
    let eventType = 'mouseover';
    let elementId = 'display-svg-1';
    let expectationValue = 2; // "display-svg-1" is the second element so the second element get a mouseover
    simulateMouseEvent(page, eventType, elementId, expectationValue);
  });
  it('onMouseLeave', async () => {
    const page = await newSpecPage({
      components: [RatingStars],
      html: `<rating-stars stars="3" selected-stars="0"></rating-stars>`,
    });
    let eventType = 'mouseleave';
    let elementId = 'display-svg-1';
    let expectationValue = 0;
    simulateMouseEvent(page, eventType, elementId, expectationValue);
  });
  it('onClick', async () => {
    const page = await newSpecPage({
      components: [RatingStars],
      html: `<rating-stars stars="3" selected-stars="0"></rating-stars>`,
    });
    let eventType = 'click';
    let elementId = 'display-svg-1';
    let expectationValue = 2; // "display-svg-1" is the second element so the second element get a click
    simulateMouseEvent(page, eventType, elementId, expectationValue);
  });
});
/**
 * - Helper function for the onMouseEvents
 * @param page = The component, in this case the rating star component
 * @param eventType = The MouseEvent that should be executed on the element
 * @param elementId = Id of the element on which the MouseEvent is to be executed
 * @param expectValueForSelectedStars = Expected number of stars after the MouseEvent has been executed
 */
async function simulateMouseEvent(page: SpecPage, eventType: string, elementId: string, expectValueForSelectedStars: any) {
  var event = new MouseEvent(eventType, {
    view: window,
    bubbles: true,
    cancelable: true,
  });
  var myTarget = page.root.shadowRoot.getElementById(elementId);
  var canceled = !myTarget.dispatchEvent(event);
  if (canceled) {
    alert('canceled');
  } else {
    alert('not canceled');
    expect(page.rootInstance.selectedStarValue).toBe(expectValueForSelectedStars);
  }
}
