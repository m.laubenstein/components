import { newE2EPage } from '@stencil/core/testing';

describe('init test', () => {
  it('should render a rating stars component', async () => {
    const page = await newE2EPage();
    await page.setContent(`<rating-stars></rating-stars>`);
    const el = await page.find('rating-stars');
    expect(el).not.toBeNull();
  });
});

describe('action test', () => {
  it('onClick', async () => {
    const page = await newE2EPage();
    await page.setContent(`<rating-stars></rating-stars>`);

    const component = await page.find('rating-stars');
    expect(await component.getProperty('rating')).toEqual(undefined);

    const el = await page.find('rating-stars >>> #display-svg-1');
    await el.click();
    expect(await component.getProperty('rating')).toEqual(2);
  });
});
