import { Component, h, Prop, State, Host } from '@stencil/core';

const sizes = {
  large: 24,
  small: 12,
};

@Component({
  tag: 'rating-stars',
  styleUrl: './rating-stars.css',
  shadow: true,
})
export class RatingStars {
  /** (optional) Total number of stars */
  @Prop({ mutable: true }) stars: number = 5;
  /** (optional) Number of stars, that are filled */
  @Prop({ reflect: true }) selectedStars: number = 0;
  /** Return value of selected stars*/
  @Prop({ mutable: true, reflect: true }) rating: number;
  /** (optional) If `true`, the stars are interactive and clickable*/
  @Prop({ reflect: true }) interactive: boolean = true;
  /** [Option1] Change a preset size*/
  @Prop({ reflect: true }) size: 'small' | 'large';
  /** [Option2] Set a individual size*/
  @Prop() individualSize: string;
  /** (optional) Chose between active and disable style*/
  @Prop() activityState: 'active' | 'disable' = 'active';
  /** (optional) Set spacing between each star*/
  @Prop() spaceBetweenStars: string = '2';
  /** (optional) The icon svg, if not set it is a normal star*/
  @Prop() getIconCommon = () => '<scale-icon-rating-star filled=false ></scale-icon-rating-star>';
  @Prop() getIconHovered = () => '<scale-icon-rating-star filled=true ></scale-icon-rating-star>';

  @State() ratingSet: boolean = false;
  @State() starValue: number;
  @State() selectedStarValue: number;

  componentWillLoad() {
    this.starValue = this.stars;
    this.selectedStarValue = this.selectedStars;
  }

  getRatingStars(numberOfStars: number, numberOfSelectedStars: number) {
    return Array.from(Array(this.stars).keys()).map(el => (
      <span
        class={'star-container'}
        id={'display-svg-' + el + ''}
        onMouseOver={() => this.handleMouseEvent('onMouseOver', numberOfStars, el + 1)}
        onMouseLeave={() => this.handleMouseEvent('onMouseLeave', numberOfStars, el + 1)}
        onClick={() => this.handleMouseEvent('onClick', numberOfStars, el + 1)}
      >
        {this.getStarSVG(el, numberOfSelectedStars)}
      </span>
    ));
  }

  getStarSVG(position: number, numberOfSelectedStars: number) {
    if (position + 1 < numberOfSelectedStars || position + 1 == numberOfSelectedStars) {
      return <span class={'svg-hover svg-hover--state-' + this.activityState} innerHTML={this.getIconHovered()}></span>;
    }
    return <span class={'svg svg--state-' + this.activityState} innerHTML={this.getIconCommon()}></span>;
  }

  handleMouseEvent(actionName: string, chosenStarValue: number, hoveredStarNumber: number) {
    if (this.interactive) {
      switch (actionName) {
        case 'onMouseOver':
          this.setCurrentStarValue(chosenStarValue, hoveredStarNumber);
          break;
        case 'onMouseLeave':
          !this.ratingSet ? this.setCurrentStarValue(chosenStarValue, this.selectedStars) : this.setCurrentStarValue(chosenStarValue, this.rating);
          break;
        case 'onClick':
          this.setCurrentStarValue(chosenStarValue, hoveredStarNumber);
          this.setRating(this.selectedStarValue);
          break;
      }
    }
  }

  setCurrentStarValue(starValue: number, selectedStarValue: number) {
    this.starValue = starValue;
    this.selectedStarValue = selectedStarValue;
  }

  setRating(starValue: number) {
    this.rating = starValue;
    this.ratingSet = true;
  }

  styles() {
    return `:host {
      --star-space-between: ${this.spaceBetweenStars}px;
      --star-size: ${this.individualSize ? this.individualSize : sizes[this.size]}px
    `;
  }

  render() {
    return (
      <Host>
        <style>{this.styles()}</style>
        <div class="rating-star-container">{this.getRatingStars(this.starValue, this.selectedStarValue)}</div>
      </Host>
    );
  }
}
